﻿namespace TicTacToe

module Main = 
    open FParsec
    open FSharp.Data
    open BencodeMap
    open BencodeList

    let post id player content contentType = 
        Http.RequestString("http://tictactoe.homedir.eu/game/"+id+"/player/"+player, body = TextRequest content, headers = ["Content-Type", contentType]) |> ignore

    let get id player contentType = 
        Http.RequestString("http://tictactoe.homedir.eu/game/"+id+"/player/"+player, headers = ["Accept", contentType])

    [<EntryPoint>]
    let main argv = 
        if argv.Length <> 2 then failwith "must pass 2 paramters"
        if argv.[0] = "POST" then
            let id = argv.[1]
            let player = "1"
            let ct = "application/bencode+map"
            let m = []
            post id player (m |> BencodeMap.makeMove |> toBencodeMap) ct
            let m = BencodeMap.fromBencodeMap(get id player ct)
            post id player (m |> BencodeMap.makeMove |> toBencodeMap) ct
            let m = BencodeMap.fromBencodeMap(get id player ct)
            post id player (m |> BencodeMap.makeMove |> toBencodeMap) ct
            let m = BencodeMap.fromBencodeMap(get id player ct)
            post id player (m |> BencodeMap.makeMove |> toBencodeMap) ct
            let m = BencodeMap.fromBencodeMap(get id player ct)
            post id player (m |> BencodeMap.makeMove |> toBencodeMap) ct
        elif argv.[0] = "GET" then
            let id = argv.[1]
            let player = "2"
            let ct = "application/bencode+list"
            let m = BencodeList.fromBencodeList(get id player ct)
            post id player (m |> BencodeList.makeMove |> toBencodeList) ct
            let m = BencodeList.fromBencodeList(get id player ct)
            post id player (m |> BencodeList.makeMove |> toBencodeList) ct
            let m = BencodeList.fromBencodeList(get id player ct)
            post id player (m |> BencodeList.makeMove |> toBencodeList) ct
            let m = BencodeList.fromBencodeList(get id player ct)
            post id player (m |> BencodeList.makeMove |> toBencodeList) ct
        0 // return an integer exit code
