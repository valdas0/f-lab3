﻿namespace TicTacToe
module BencodeMap = 
    
    open FParsec

    type Move = {
        Index : char;
        Value : char;
        X : int;
        Y : int
    }

    let parse p str =
        match run p str with
        | Success(result, _, _)   -> result
        | Failure(errorMsg, _, _) -> failwith errorMsg

    let bencodeString = skipString "1:" >>. anyChar
    let bencodeNumber = skipString "i" >>. pint32 .>> skipString "e"
    let pvalue = skipString "1:v" >>. bencodeString
    let px = skipString "1:x" >>. bencodeNumber
    let py = skipString "1:y" >>. bencodeNumber
    let pMove = pipe4 (bencodeString .>> skipString "d") pvalue px py (fun i v x y -> {Index = i; Value = v; X = x; Y = y}) .>> skipString "e"
    let pMoves = skipString "d" >>. many pMove .>> skipString "e"

    let fromBencodeMap str = parse pMoves str

    let toBencodeMap moves = 
        let rec inner = function
            | [] -> ""
            | {Index = i; Value = v; X = x; Y = y} :: tail -> (sprintf "1:%cd1:v1:%c1:xi%ie1:yi%iee" i v x y) + inner tail
        "d" + (inner moves) + "e"

    let sameCoordinates a b =
        a.X = b.X && a.Y = b.Y
    
    let middleCoordinates a =
        a.X = 1 && a.Y = 1

    let cornerCoordinates a =
        (a.X % 2 = 0) && (a.Y % 2 = 0) 

    let findEmptySquares list = 
        let squares = [
            for x in 0..2 do
            for y in 0..2 do
            yield {Index = '-'; Value = '-'; X = x; Y = y}
        ]
        List.filter (fun s1 -> List.exists (fun s2 -> sameCoordinates s1 s2) list |> not) squares

    let nextSquare list =
        let emptySquares = findEmptySquares list
        let middleSquares = List.filter middleCoordinates emptySquares
        let cornerSquares = List.filter cornerCoordinates emptySquares
        if emptySquares.IsEmpty then failwith "No more possible moves"
        elif not middleSquares.IsEmpty then middleSquares.Head
        elif not cornerSquares.IsEmpty then cornerSquares.Head
        else emptySquares.Head

    let makeMove list =
        let next = nextSquare list
        let v = if list.Length % 2 = 0 then 'X' else 'O'
        List.append list [{next with Value = v; Index = list.Length.ToString().[0]}]