﻿namespace TicTacToe
module BencodeList = 
    
    open FParsec

    type Move = {
        Value : char;
        X : int;
        Y : int
    }

    let parse p str =
        match run p str with
        | Success(result, _, _)   -> result
        | Failure(errorMsg, _, _) -> failwith errorMsg

    let bencodeString = skipString "1:" >>. anyChar
    let bencodeNumber = skipString "i" >>. pint32 .>> skipString "e"
    let pvalue = skipString "1:v" >>. bencodeString
    let px = skipString "1:x" >>. bencodeNumber
    let py = skipString "1:y" >>. bencodeNumber
    let pMove = skipString "d" >>. pipe3 pvalue px py (fun v x y -> {Value = v; X = x; Y = y}) .>> skipString "e"
    let pMoves = skipString "l" >>. many pMove .>> skipString "e"

    let fromBencodeList str = parse pMoves str

    let toBencodeList moves = 
        let rec inner = function
            | [] -> ""
            | {Value = v; X = x; Y = y} :: tail -> (sprintf "d1:v1:%c1:xi%ie1:yi%iee" v x y) + inner tail
        "l" + (inner moves) + "e"
        
    let sameCoordinates a b =
        a.X = b.X && a.Y = b.Y
    
    let middleCoordinates a =
        a.X = 1 && a.Y = 1

    let cornerCoordinates a =
        (a.X % 2 = 0) && (a.Y % 2 = 0) 

    let findEmptySquares list = 
        let squares = [
            for x in 0..2 do
            for y in 0..2 do
            yield {Value = '-'; X = x; Y = y}
        ]
        List.filter (fun s1 -> List.exists (fun s2 -> sameCoordinates s1 s2) list |> not) squares

    let nextSquare list =
        let emptySquares = findEmptySquares list
        let middleSquares = List.filter middleCoordinates emptySquares
        let cornerSquares = List.filter cornerCoordinates emptySquares
        if emptySquares.IsEmpty then failwith "No more possible moves"
        elif not middleSquares.IsEmpty then middleSquares.Head
        elif not cornerSquares.IsEmpty then cornerSquares.Head
        else emptySquares.Head

    let makeMove list =
        let next = nextSquare list
        let v = if list.Length % 2 = 0 then 'X' else 'O'
        List.append list [{next with Value = v}]